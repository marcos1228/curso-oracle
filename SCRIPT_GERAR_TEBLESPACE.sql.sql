--Criando a sua Proprio TableSpace 
CREATE TABLESPACE Curso
DATAFILE 
'C:\oraclexe\app\oracle\oradata\XE\Curso.dbf'
SIZE 100m AUTOEXTEND ON next 50m MAXSIZE 500m
online
permanent 
extent management local autoallocate 
segment space management auto;

--criando usuario 
CREATE USER aluno
        identified by aluno
        default tablespace Curso
        temporary tablespace TEMP;
        
        
--Permiss�o para Aluno
GRANT CREATE SESSION, CONNECT, RESOURCE TO aluno;


ALTER USER aluno QUOTA UNLIMITED ON curso;

--Delete user
DROP TABLESPACE Curso
    INCLUDING CONTENTS AND DATAFILES
    CASCADE CONSTRAINTS;





